---
name: Important Dates
---
<style type="text/css">
tr td:first-child {
  width: 10rem;
}
</style>


| **MONTH** |                                                                   |
|-----------|-------------------------------------------------------------------|
| FIXME     | Opening of the [Call For Proposals](/cfp/).                       |
| FIXME     | Opening attendee registration.                                    |
| FIXME     | Last date to apply for a bursary.                                 |
| FIXME     | Last day for submitting a talk that will be surely considered for the official schedule and have a response by July 23rd. |
| FIXME     | Acceptance notifications for talks submitted before July 9 will be sent around this day. |
| FIXME     | Last day for bursary team review travel requests and make 1st round of offers to bursary recipients. |
| FIXME     | Last day for 1st round of bursary recipients to accept.           |
| FIXME     | Last date to let us know if you need a visa to South Korea (email: visa@debconf.org with a copy of your passport). |
| FIXME     | Last day to register with guaranteed swag. Registrations after this date are still possible, but swag is not guaranteed. |
| FIXME     | Last day to submit to proposal be considered for the main conference schedule, with video coverage guaranteed. |

| **JULY**       |                                                  |
|----------------|--------------------------------------------------|
| *DebCamp*      |                                                  |
| 21 (Sunday)    | First day of DebCamp / Arrival day for DebCamp   |
| 22 (Monday)    | Second day of DebCamp                            |
| 23 (Tuesday)   | Third day of DebCamp                             |
| 24 (Wednesday) | Fourth day of DebCamp                            |
| 25 (Thursday)  | Fifth day of DebCamp                             |
| 26 (Friday)    | Sixth day of DebCamp                             |
| 27 (Saturday)  | Seventh day of DebCamp / Arrival day for DebConf |
| *DebConf*      |                                                  |
| 28 (Sunday)    | First day of DebConf / Opening / Job fair        |
| 29 (Monday)    | Second day of DebConf / Cheese and wine party    |
| 30 (Tuesday)   | Third day of DebConf / Sadya (Four Points)       |
| 31 (Wednesday) | Fourth day of DebConf / Day trip                 |

| **AUGUST**  |                                                             |
|----------------|----------------------------------------------------------|
| 01 (Thursday)  | Fifth day of DebConf / Conference dinner (Bolgatty)      |
| 02 (Friday)    | Sixth day of DebConf                                     |
| 03 (Saturday)  | Seventh day of DebConf                                   |
| 04 (Sunday)    | Last day of DebConf / Closing ceremony                   |
| 05 (Monday)    | Departure day                                            |
