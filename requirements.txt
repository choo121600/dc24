Django>=4,<5

# Available in Debian
PyYAML
django-countries
# https://github.com/SmileyChris/django-countries/issues/410
# django-countries should depende on this:
asgiref
psycopg2-binary
pymemcache

# From the cheeseshop
bleach
bleach-allowlist
django-crispy-forms>=2
mdx_linkify==2.1
mdx_staticfiles==0.1
stripe==2.60.0
wafer==0.16.0
wafer-debconf==0.14.0

# unused but a dependency of django-bakery
# pinned to avoid having to update it every day
boto3==1.18.44
botocore==1.21.44
